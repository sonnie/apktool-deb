Format: 3.0 (quilt)
Source: apktool
Binary: apktool
Architecture: all
Version: 2.5.0+dfsg.1-2
Maintainer: Android Tools Maintainers <android-tools-devel@lists.alioth.debian.org>
Uploaders:  Markus Koschany <apo@debian.org>
Homepage: https://ibotpeaches.github.io/Apktool/
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/android-tools-team/apktool
Vcs-Git: https://salsa.debian.org/android-tools-team/apktool.git
Build-Depends: antlr3, debhelper-compat (= 12), default-jdk, gradle-debian-helper, junit4, libantlr3-runtime-java, libcommons-cli-java, libcommons-io-java, libcommons-lang3-java, libguava-java, libjgit-java, libsmali-java (>= 2.4.0), libstringtemplate-java, libxmlunit-java, libxpp3-java, libyaml-snake-java, proguard-cli
Package-List:
 apktool deb devel optional arch=all
Checksums-Sha1:
 81cd934a5b50e147849caf17c5d561d7a41dae63 7635724 apktool_2.5.0+dfsg.1.orig.tar.xz
 86817b070b4107532ee0abee42ef188191d6077e 8788 apktool_2.5.0+dfsg.1-2.debian.tar.xz
Checksums-Sha256:
 054c46023136067e54978e865106af0085825f880e708b0278892eafd1e70cf6 7635724 apktool_2.5.0+dfsg.1.orig.tar.xz
 d8100ad85f5dd9a9e4cc66a4582ffa3c56a14f4b86e5aeca4623824762171fc5 8788 apktool_2.5.0+dfsg.1-2.debian.tar.xz
Files:
 f10ae27aa6260e3820660b6b33531923 7635724 apktool_2.5.0+dfsg.1.orig.tar.xz
 04ffe9a2bf8a6df8960688802117d1f7 8788 apktool_2.5.0+dfsg.1-2.debian.tar.xz
